package com.jeeranan132.week11;

public interface Walkable {
    public void walk();

    public void run();
}
