package com.jeeranan132.week11;

public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();

        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        Bat bat1 = new Bat("Chicky");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Snake snake1 = new Snake("KingCobra");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();
        snake1.swim();

        Rat rat1 = new Rat("Tom");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        Dog dog1 = new Dog("Dobby");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();

        Cat cat1 = new Cat("Kitty");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        cat1.swim();

        System.out.println();
        System.out.println("<< Flyable Link >>");
        Flyable[] flyables = { bird1, boeing, clark, bat1 };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        System.out.println();
        System.out.println("<< Walking Link >>");
        Walkable[] walkables = { bird1, clark, man1, rat1, dog1, cat1 };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println();
        System.out.println("<< Swimable Link >>");
        Swimable[] swimable = { clark, man1, snake1, dog1, cat1 };
        for (int i = 0; i < swimable.length; i++) {
            swimable[i].swim();
        }

        System.out.println();
        System.out.println("<< Crawl Link >>");
        Crawlable[] crawlables = { snake1 };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }

    }
}
