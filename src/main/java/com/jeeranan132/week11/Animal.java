package com.jeeranan132.week11;

public abstract class Animal {
    private String name;
    private int numOfLeg;

    public Animal(String name, int numOfLeg) {
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName() {
        return this.name;
    }

    public int getNumberOfLeg() {
        return this.numOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfLeg(int numOfLeg) {
        this.numOfLeg = numOfLeg;
    }

    @Override
    public String toString() {
        return "Animal(" + name + ")";
    }

    public abstract void sleep();

    public abstract void eat();
}
