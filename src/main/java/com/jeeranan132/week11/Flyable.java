package com.jeeranan132.week11;

public interface Flyable {
    public void takeoff();

    public void fly();

    public void landing();
}
